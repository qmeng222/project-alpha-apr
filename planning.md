## Project Alpha_Qingying Meng_8/1/2022

---

## Resources:

https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/66-assessment-project.md

---

## Feature 1: install dependencies

- [x] Fork and clone the starter project from django-one-shot:
      git clone https://gitlab.com/qmeng222/project-alpha-apr.git
- [x] Create a new virtual environment in the repository directory for the project: python -m venv .venv
- [x] Activate the virtual environment: source .venv/bin/activate
- [x] Upgrade pip: python -m pip install --upgrade pip
- [x] Install django: pip install Django
- [x] Install black: pip install black
- [x] Install flake8: pip install flake8
- [x] Install djlint: pip install djhtml
- [x] Install Django debug toolbar: pip install django-debug-toolbar
- [x] Deactivate your virtual environment: deactivate
- [x] Activate your virtual environment: source .venv/bin/activate
- [x] Use pip freeze to generate a requirements.txt file: pip freeze > requirements.txt
- [x] test feature: python -m unittest tests.test_feature_01
- [x] Add and commit your progress

---

## Feature 2: have a Django project named tracker, and three Django apps named accounts, projects, and tasks.

- [x] create a Django project named tracker: django-admin startproject tracker .
- [x] (1). create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list:
      python manage.py startapp accounts
      INSTALLED_APPS = [
      "accounts.apps.AccountsConfig",
      ...
      ]
- [x] (2). create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
- [x] (3). create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
- [x] run the migrations: python manage.py makemigrations, python manage.py migrate
- [x] create super user: python manage.py createsuperuser
- [x] test feature: python manage.py test tests.test_feature_02
- [x] add & commit & push: git commit -m "Feature 2 complete"

---

## Feature 3: Project model

- [x] create a Project model in the projects Django app
- [x] migrate
- [x] test feature
- [x] push

---

## Feature 4: register Project in the admin

- [x] register the Project model with the admin so that you can see it in the Django admin site
- [x] test feature
- [x] push

---

## Feature 5: Project list view

- [x] create a view that will get all of the instances of the Project model and puts them in the context for the template
- [x] register that view in the projects app for the path "" and the name "list_projects" in a new file named projects/urls.py
- [x] include the URL patterns from the projects app in the tracker project with the prefix "projects/"
- [x] create a template for the list view that complies with the specifications at https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/71-assessment-project.md
- [x] test feature
- [x] push

---

## Feature 6: default path redirect

- [x] redirect http://localhost:8000/ to the project list page created in Feature 5
- [x] test feature
- [x] push

---

## Feature 7: login page

- [x] register the LoginView in your accounts urls.py with the path "login/" and the name "login"
- [x] include the URL patterns from the accounts app in the tracker project with the prefix "accounts/"
- [x] create a templates directory under accounts
- [x] create a registration directory under templates
- [x] create an HTML template named login.html in the registration directory
- [x] put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications at https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/73-assessment-project.md)
- [x] in the tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"
- [x] test feature
- [x] push

---

## Feature 8: require login for Project list view

- [x] protect the list view for the Project model so that only a person that has logged in can access it
- [x] change the queryset of the view to filter the Project objects where members equals the logged in user

---

## Feature 9: logout page

- [x] in the accounts/urls.py file:
  - [x] import the LogoutView from the same module that you imported the LoginView from
  - [x] register that view in your urlpatterns list with the path "logout/" and the name "logout"
- [x] in the tracker settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page
- [x] test feature
- [x] push

---

## Feature 10: allow sign up for the project tracker

- [x] import the UserCreationForm from the built-in auth forms
- [x] use the special create_user method to create a new user account from their username and password
- [x] use the login function that logs an account in
- [x] after you have created the user, redirect the browser to the path registered with the name "home"
- [x] create an HTML template named signup.html in the registration directory
- [x] put a post form in the signup.html and any other required HTML or template inheritance stuff
- [x] update accounts urls.py
- [x] test feature
- [x] push

---

## Feature 11: in **_tasks_** app, create a Task model

- [x] the Task model should have attributes at https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/77-assessment-project.md
- [x] dunder: class Task should implicitly convert to a string that is the value of the name property.
- [x] test feature
- [x] push

---

## Feature 12: register Task in the admin

- [x] in **_tasks_** app:
  - [x] register the Task model with the tasks.admin, so that you can see it in the Django admin site.
  - [x] test feature
  - [x] push

---

## Feature 13: Project detail view

- [x] create a view that shows the details of a particular project
- [x] a user must be logged in to see the view
- [x] in the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
- [x] create a template projects > detail.html to show the project name, project description, and a table of its tasks
- [x] update the list template to show the number of tasks for a project
- [x] update the list template to have a link from the project name to the detail view for that project
- [x] test feature
- [x] push

---

## Feature 14: Project create view

- [x] create a CreateView for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
- [x] a person must be logged in to see the view
- [x] if the project is successfully created, it should redirect to the detail page for that project
- [x] register that view for the path "create/" in the projects urls.py and the name "create_project"
- [x] create an HTML template that shows the form to create a new Project meeting specifications at https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/80-assessment-project.md
- [x] add a link to the list view for the Project that navigates to the new create view
- [x] test feature
- [x] push

---

## Feature 15: Task create view

- [x] create a view that will show a form to create an instance of the Task model for all properties except the is_completed field
- [x] the view must only be accessible by people that are logged in
- [x] when the view successfully handles the form submission, have it redirect to the detail page of the task's project
- [x] register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
- [x] include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
- [x] create a template for the create view that complies with the specifications at https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/81-assessment-project.md
- [x] add an <a> tag with an href attribute that points to the "create_task" path with the content "Create a new task" at the project detail page
- [x] test feature
- [x] push

---

## Feature 16: Show "My Tasks" list view

- [x] create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user
- [x] the view must only be accessible by people that are logged in
- [x] register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
- [ ] create an HTML template that conforms with the specifications at https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/82-assessment-project.md
- [x] test feature
- [x] push

---

## Feature 17: completing a task

- [x] create an update view for the Task model that only is concerned with the is_completed field
- [x] when the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My Tasks" view (success_url property on a view class)
- [x] register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
- [x] you do not need to make a template for this view
- [x] modify the "My Tasks" view to comply with the template specification at https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/66-assessment-project.md
- [x] test feature
- [x] push

---

## Feature 18: markdownify

- [x] install Django Markdownify with pip: pip install django-markdownify
- [x] add markdownify to INSTALLED_APPS in tracker > settings.py:
- [x] in the tracker settings.py file, add:
      MARKDOWNIFY = {
      "default": {
      "BLEACH": False
      }
      }
- [x] in projects > detail.html, load the markdownify template library: {% load markdownify %}
- [x] replace the p tag and {{ project.description }} in the Project detail view with {{ project.description|markdownify }}
- [x] use pip freeze to update requirements.txt file: pip freeze > requirements.txt
  - [x] Markdown==3.4.1 will be listed after the update
- [x] test feature: python manage.py test tests.test_feature_18
- [x] add, commit, and push:
  - [x] git add .
  - [x] git commit -m "Feature 18 complete"
  - [x] git push origin main

---

## Feature 19: add navigation to projects > base.html

- [x] on base.html, add a navigation comply with the template specification at https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/85-assessment-project.md

---

## Project Submission:

- [x] format your code: black accounts projects tasks tracker
- [x] run this command for each HTML template: djhtml -i «path to HTML template»
- [x] run this command to find problems in your code: flake8 --select F401,F403,F541,F601,F631,F632,F634,F701,F702,F703,F706,F707,F811,F821,F823,F831,F841 projects accounts tasks tracker
  - [x] if this returns errors in files that Django generated for you, like tests.py in your Django apps that you haven't written any tests in, or a models.py file that you didn't use, delete those empty files: git rm «path/to/file.py»
- [x] double-check that tests are passing: python manage.py test
- [ ] add, commit, and push: git commit -m "Final submission"
